<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class OEscritorio extends Model
{
    protected $table = 'o_escritorio';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 450,
            'height' => null,
            'path'   => 'assets/img/o-escritorio/'
        ]);
    }

}
