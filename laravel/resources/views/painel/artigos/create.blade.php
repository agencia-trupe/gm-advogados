@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Artigos /</small> Adicionar Artigo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.artigos.store', 'files' => true]) !!}

        @include('painel.artigos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
