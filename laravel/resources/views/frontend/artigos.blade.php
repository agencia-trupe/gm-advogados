@extends('frontend.common.template')

@section('content')

    <div class="main atuacao-artigos">
        <div class="center">
            <h2>ARTIGOS</h2>

            <div class="lista">
                @foreach($artigos as $titulo => $slug)
                <a href="{{ route('artigos', $slug) }}" class="link-aside @if($selecao-> slug === $slug) active @endif">
                    {{ $titulo }}
                </a>
                @endforeach
            </div>

            <div class="texto">
                {!! $selecao->texto !!}
            </div>
        </div>
    </div>

@endsection
