<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OEscritorioRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'chamada' => 'required',
            'texto' => 'required',
            'imagem' => 'image',
        ];
    }
}
