<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AreasDeAtuacaoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'slug' => '',
            'texto' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
