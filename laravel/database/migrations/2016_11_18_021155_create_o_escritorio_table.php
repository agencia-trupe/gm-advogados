<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOEscritorioTable extends Migration
{
    public function up()
    {
        Schema::create('o_escritorio', function (Blueprint $table) {
            $table->increments('id');
            $table->string('chamada');
            $table->text('texto');
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('o_escritorio');
    }
}
