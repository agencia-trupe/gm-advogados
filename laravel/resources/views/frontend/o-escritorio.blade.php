@extends('frontend.common.template')

@section('content')

    <div class="main escritorio">
        <div class="center">
            <h2>O ESCRITÓRIO</h2>

            <div class="chamada">
                <p>{{ $escritorio->chamada }}</p>
                <img src="{{ asset('assets/img/o-escritorio/'.$escritorio->imagem) }}" alt="">
            </div>

            <div class="texto">
                <div>{!! $escritorio->texto !!}</div>

                <div class="formulario-contato">
                    <h4>FALE CONOSCO</h4>
                    <form action="" id="form-contato" method="POST">
                        <input type="text" name="nome" id="nome" placeholder="nome" required>
                        <input type="email" name="email" id="email" placeholder="e-mail" required>
                        <input type="text" name="telefone" id="telefone" placeholder="telefone">
                        <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                        <input type="submit" value="ENVIAR">
                        <div id="form-contato-response"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
