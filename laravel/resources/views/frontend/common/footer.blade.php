    <footer>
        <div class="center">
            <div class="info">
                <p class="endereco">{!! $contato->endereco !!}</p>
                <p class="telefones">
                    @foreach(Tools::telefones($contato->telefones) as $telefone)
                    <span>{{ $telefone }}</span>
                    @endforeach
                </p>
            </div>

            <nav>
                @include('frontend.common._nav')
            </nav>

            <div class="copyright">
                @if($contato->facebook)
                <a href="{{ $contato->facebook }}" class="facebook">facebook</a>
                @endif

                <p>
                    © {{ date('Y') }} G&M Advogados Associados.<br>
                    Todos os direitos reservados
                </p>
            </div>
        </div>
    </footer>
