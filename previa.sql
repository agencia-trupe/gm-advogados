-- MySQL dump 10.13  Distrib 5.7.11, for Linux (x86_64)
--
-- Host: localhost    Database: gm-advogados
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `advogados`
--

DROP TABLE IF EXISTS `advogados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advogados` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `e_mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `celular` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advogados`
--

LOCK TABLES `advogados` WRITE;
/*!40000 ALTER TABLE `advogados` DISABLE KEYS */;
INSERT INTO `advogados` VALUES (1,1,'CRISTIANO FERREIRA GALRÃO','cristiano@cgrm.adv.br','','','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt tortor sit amet est laoreet, a venenatis tellus suscipit. Etiam ut sapien sapien. Quisque vulputate, turpis vitae dictum facilisis, augue metus pulvinar elit, sagittis mollis orci sem ut ex. Morbi quis tristique sem. Pellentesque ultricies sed nibh vel fringilla. Praesent et mauris congue, lobortis libero nec, dignissim leo. Nullam posuere urna eu eros ultricies, vel aliquet massa viverra.</p>\r\n\r\n<p>Quisque a augue blandit purus ullamcorper ultrices. Donec molestie risus sit amet nisl vulputate, id consequat odio lobortis. Sed nisi erat, luctus in malesuada sit amet, rhoncus non ligula. Sed tortor nisi, gravida sit amet nisl in, ultrices sollicitudin tortor. Maecenas tempor justo at pellentesque euismod. Sed vel lacus sit amet sapien ultricies interdum et volutpat ex. Nunc faucibus lectus at justo pellentesque, at accumsan tellus ultricies.</p>\r\n','2016-11-18 18:31:37','2016-11-18 18:31:37'),(2,2,'ROGÉRIO MACHTANS','rogerio@cgrm.adv.br','11 2936 9130','11 9 8193 1247','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt tortor sit amet est laoreet, a venenatis tellus suscipit. Etiam ut sapien sapien. Quisque vulputate, turpis vitae dictum facilisis, augue metus pulvinar elit, sagittis mollis orci sem ut ex. Morbi quis tristique sem. Pellentesque ultricies sed nibh vel fringilla. Praesent et mauris congue, lobortis libero nec, dignissim leo. Nullam posuere urna eu eros ultricies, vel aliquet massa viverra.</p>\r\n\r\n<p>Quisque a augue blandit purus ullamcorper ultrices. Donec molestie risus sit amet nisl vulputate, id consequat odio lobortis. Sed nisi erat, luctus in malesuada sit amet, rhoncus non ligula. Sed tortor nisi, gravida sit amet nisl in, ultrices sollicitudin tortor. Maecenas tempor justo at pellentesque euismod. Sed vel lacus sit amet sapien ultricies interdum et volutpat ex. Nunc faucibus lectus at justo pellentesque, at accumsan tellus ultricies.</p>\r\n','2016-11-18 18:31:56','2016-11-18 18:31:56');
/*!40000 ALTER TABLE `advogados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `areas_de_atuacao`
--

DROP TABLE IF EXISTS `areas_de_atuacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `areas_de_atuacao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `areas_de_atuacao`
--

LOCK TABLES `areas_de_atuacao` WRITE;
/*!40000 ALTER TABLE `areas_de_atuacao` DISABLE KEYS */;
INSERT INTO `areas_de_atuacao` VALUES (1,0,'Treinamento em Recursos Humanos','treinamento-em-recursos-humanos','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt tortor sit amet est laoreet, a venenatis tellus suscipit. Etiam ut sapien sapien. Quisque vulputate, turpis vitae dictum facilisis, augue metus pulvinar elit, sagittis mollis orci sem ut ex. Morbi quis tristique sem.</p>\r\n\r\n<p><a href=\"#\">Exemplo de link</a></p>\r\n\r\n<p>Pellentesque ultricies sed nibh vel fringilla. Praesent et mauris congue, lobortis libero nec, dignissim leo. Nullam posuere urna eu eros ultricies, vel aliquet massa viverra. Quisque a augue blandit purus ullamcorper ultrices. Donec molestie risus sit amet nisl vulputate, id consequat odio lobortis.</p>\r\n\r\n<ul>\r\n	<li>Sed nisi erat, luctus in malesuada sit amet, rhoncus non ligula.</li>\r\n	<li>Sed tortor nisi, gravida sit amet nisl in, ultrices sollicitudin tortor.</li>\r\n</ul>\r\n\r\n<p>Maecenas tempor justo at pellentesque euismod. Sed vel lacus sit amet sapien ultricies interdum et volutpat ex. Nunc faucibus lectus at justo pellentesque, at accumsan tellus ultricies.</p>\r\n\r\n<p>Sed vestibulum tempus pharetra. Duis at nibh at nisl laoreet imperdiet. Proin vitae mi vel enim dignissim ultrices et aliquet ex. Vestibulum sollicitudin tortor sagittis lacus commodo mattis. Aliquam augue diam, tempus non dolor tristique, varius commodo nibh.</p>\r\n','2016-11-18 18:32:48','2016-11-18 18:32:48'),(2,0,'Família e Sucessões','familia-e-sucessoes','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt tortor sit amet est laoreet, a venenatis tellus suscipit. Etiam ut sapien sapien. Quisque vulputate, turpis vitae dictum facilisis, augue metus pulvinar elit, sagittis mollis orci sem ut ex. Morbi quis tristique sem.</p>\r\n\r\n<p><a href=\"#\">Exemplo de link</a></p>\r\n\r\n<p>Pellentesque ultricies sed nibh vel fringilla. Praesent et mauris congue, lobortis libero nec, dignissim leo. Nullam posuere urna eu eros ultricies, vel aliquet massa viverra. Quisque a augue blandit purus ullamcorper ultrices. Donec molestie risus sit amet nisl vulputate, id consequat odio lobortis.</p>\r\n\r\n<ul>\r\n	<li>Sed nisi erat, luctus in malesuada sit amet, rhoncus non ligula.</li>\r\n	<li>Sed tortor nisi, gravida sit amet nisl in, ultrices sollicitudin tortor.</li>\r\n</ul>\r\n\r\n<p>Maecenas tempor justo at pellentesque euismod. Sed vel lacus sit amet sapien ultricies interdum et volutpat ex. Nunc faucibus lectus at justo pellentesque, at accumsan tellus ultricies.</p>\r\n\r\n<p>Sed vestibulum tempus pharetra. Duis at nibh at nisl laoreet imperdiet. Proin vitae mi vel enim dignissim ultrices et aliquet ex. Vestibulum sollicitudin tortor sagittis lacus commodo mattis. Aliquam augue diam, tempus non dolor tristique, varius commodo nibh.</p>\r\n','2016-11-18 18:32:56','2016-11-18 18:32:56'),(3,0,'Direito Securitário','direito-securitario','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt tortor sit amet est laoreet, a venenatis tellus suscipit. Etiam ut sapien sapien. Quisque vulputate, turpis vitae dictum facilisis, augue metus pulvinar elit, sagittis mollis orci sem ut ex. Morbi quis tristique sem.</p>\r\n\r\n<p><a href=\"#\">Exemplo de link</a></p>\r\n\r\n<p>Pellentesque ultricies sed nibh vel fringilla. Praesent et mauris congue, lobortis libero nec, dignissim leo. Nullam posuere urna eu eros ultricies, vel aliquet massa viverra. Quisque a augue blandit purus ullamcorper ultrices. Donec molestie risus sit amet nisl vulputate, id consequat odio lobortis.</p>\r\n\r\n<ul>\r\n	<li>Sed nisi erat, luctus in malesuada sit amet, rhoncus non ligula.</li>\r\n	<li>Sed tortor nisi, gravida sit amet nisl in, ultrices sollicitudin tortor.</li>\r\n</ul>\r\n\r\n<p>Maecenas tempor justo at pellentesque euismod. Sed vel lacus sit amet sapien ultricies interdum et volutpat ex. Nunc faucibus lectus at justo pellentesque, at accumsan tellus ultricies.</p>\r\n\r\n<p>Sed vestibulum tempus pharetra. Duis at nibh at nisl laoreet imperdiet. Proin vitae mi vel enim dignissim ultrices et aliquet ex. Vestibulum sollicitudin tortor sagittis lacus commodo mattis. Aliquam augue diam, tempus non dolor tristique, varius commodo nibh.</p>\r\n','2016-11-18 18:33:05','2016-11-18 18:33:05'),(4,0,'Direito Societário','direito-societario','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt tortor sit amet est laoreet, a venenatis tellus suscipit. Etiam ut sapien sapien. Quisque vulputate, turpis vitae dictum facilisis, augue metus pulvinar elit, sagittis mollis orci sem ut ex. Morbi quis tristique sem.</p>\r\n\r\n<p><a href=\"#\">Exemplo de link</a></p>\r\n\r\n<p>Pellentesque ultricies sed nibh vel fringilla. Praesent et mauris congue, lobortis libero nec, dignissim leo. Nullam posuere urna eu eros ultricies, vel aliquet massa viverra. Quisque a augue blandit purus ullamcorper ultrices. Donec molestie risus sit amet nisl vulputate, id consequat odio lobortis.</p>\r\n\r\n<ul>\r\n	<li>Sed nisi erat, luctus in malesuada sit amet, rhoncus non ligula.</li>\r\n	<li>Sed tortor nisi, gravida sit amet nisl in, ultrices sollicitudin tortor.</li>\r\n</ul>\r\n\r\n<p>Maecenas tempor justo at pellentesque euismod. Sed vel lacus sit amet sapien ultricies interdum et volutpat ex. Nunc faucibus lectus at justo pellentesque, at accumsan tellus ultricies.</p>\r\n\r\n<p>Sed vestibulum tempus pharetra. Duis at nibh at nisl laoreet imperdiet. Proin vitae mi vel enim dignissim ultrices et aliquet ex. Vestibulum sollicitudin tortor sagittis lacus commodo mattis. Aliquam augue diam, tempus non dolor tristique, varius commodo nibh.</p>\r\n','2016-11-18 18:33:09','2016-11-18 18:33:09'),(5,0,'Recuperação de Crédito (Administrativa e Judicialmente)','recuperacao-de-credito-administrativa-e-judicialmente','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt tortor sit amet est laoreet, a venenatis tellus suscipit. Etiam ut sapien sapien. Quisque vulputate, turpis vitae dictum facilisis, augue metus pulvinar elit, sagittis mollis orci sem ut ex. Morbi quis tristique sem.</p>\r\n\r\n<p><a href=\"#\">Exemplo de link</a></p>\r\n\r\n<p>Pellentesque ultricies sed nibh vel fringilla. Praesent et mauris congue, lobortis libero nec, dignissim leo. Nullam posuere urna eu eros ultricies, vel aliquet massa viverra. Quisque a augue blandit purus ullamcorper ultrices. Donec molestie risus sit amet nisl vulputate, id consequat odio lobortis.</p>\r\n\r\n<ul>\r\n	<li>Sed nisi erat, luctus in malesuada sit amet, rhoncus non ligula.</li>\r\n	<li>Sed tortor nisi, gravida sit amet nisl in, ultrices sollicitudin tortor.</li>\r\n</ul>\r\n\r\n<p>Maecenas tempor justo at pellentesque euismod. Sed vel lacus sit amet sapien ultricies interdum et volutpat ex. Nunc faucibus lectus at justo pellentesque, at accumsan tellus ultricies.</p>\r\n\r\n<p>Sed vestibulum tempus pharetra. Duis at nibh at nisl laoreet imperdiet. Proin vitae mi vel enim dignissim ultrices et aliquet ex. Vestibulum sollicitudin tortor sagittis lacus commodo mattis. Aliquam augue diam, tempus non dolor tristique, varius commodo nibh.</p>\r\n','2016-11-18 18:33:27','2016-11-18 18:33:27'),(6,0,'Direito Contratual','direito-contratual','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt tortor sit amet est laoreet, a venenatis tellus suscipit. Etiam ut sapien sapien. Quisque vulputate, turpis vitae dictum facilisis, augue metus pulvinar elit, sagittis mollis orci sem ut ex. Morbi quis tristique sem.</p>\r\n\r\n<p><a href=\"#\">Exemplo de link</a></p>\r\n\r\n<p>Pellentesque ultricies sed nibh vel fringilla. Praesent et mauris congue, lobortis libero nec, dignissim leo. Nullam posuere urna eu eros ultricies, vel aliquet massa viverra. Quisque a augue blandit purus ullamcorper ultrices. Donec molestie risus sit amet nisl vulputate, id consequat odio lobortis.</p>\r\n\r\n<ul>\r\n	<li>Sed nisi erat, luctus in malesuada sit amet, rhoncus non ligula.</li>\r\n	<li>Sed tortor nisi, gravida sit amet nisl in, ultrices sollicitudin tortor.</li>\r\n</ul>\r\n\r\n<p>Maecenas tempor justo at pellentesque euismod. Sed vel lacus sit amet sapien ultricies interdum et volutpat ex. Nunc faucibus lectus at justo pellentesque, at accumsan tellus ultricies.</p>\r\n\r\n<p>Sed vestibulum tempus pharetra. Duis at nibh at nisl laoreet imperdiet. Proin vitae mi vel enim dignissim ultrices et aliquet ex. Vestibulum sollicitudin tortor sagittis lacus commodo mattis. Aliquam augue diam, tempus non dolor tristique, varius commodo nibh.</p>\r\n','2016-11-18 18:33:34','2016-11-18 18:33:34'),(7,0,'Direito Civil','direito-civil','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt tortor sit amet est laoreet, a venenatis tellus suscipit. Etiam ut sapien sapien. Quisque vulputate, turpis vitae dictum facilisis, augue metus pulvinar elit, sagittis mollis orci sem ut ex. Morbi quis tristique sem.</p>\r\n\r\n<p><a href=\"#\">Exemplo de link</a></p>\r\n\r\n<p>Pellentesque ultricies sed nibh vel fringilla. Praesent et mauris congue, lobortis libero nec, dignissim leo. Nullam posuere urna eu eros ultricies, vel aliquet massa viverra. Quisque a augue blandit purus ullamcorper ultrices. Donec molestie risus sit amet nisl vulputate, id consequat odio lobortis.</p>\r\n\r\n<ul>\r\n	<li>Sed nisi erat, luctus in malesuada sit amet, rhoncus non ligula.</li>\r\n	<li>Sed tortor nisi, gravida sit amet nisl in, ultrices sollicitudin tortor.</li>\r\n</ul>\r\n\r\n<p>Maecenas tempor justo at pellentesque euismod. Sed vel lacus sit amet sapien ultricies interdum et volutpat ex. Nunc faucibus lectus at justo pellentesque, at accumsan tellus ultricies.</p>\r\n\r\n<p>Sed vestibulum tempus pharetra. Duis at nibh at nisl laoreet imperdiet. Proin vitae mi vel enim dignissim ultrices et aliquet ex. Vestibulum sollicitudin tortor sagittis lacus commodo mattis. Aliquam augue diam, tempus non dolor tristique, varius commodo nibh.</p>\r\n','2016-11-18 18:33:37','2016-11-18 18:33:37'),(8,0,'Direito do Trabalho (Auditoria)','direito-do-trabalho-auditoria','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt tortor sit amet est laoreet, a venenatis tellus suscipit. Etiam ut sapien sapien. Quisque vulputate, turpis vitae dictum facilisis, augue metus pulvinar elit, sagittis mollis orci sem ut ex. Morbi quis tristique sem.</p>\r\n\r\n<p><a href=\"#\">Exemplo de link</a></p>\r\n\r\n<p>Pellentesque ultricies sed nibh vel fringilla. Praesent et mauris congue, lobortis libero nec, dignissim leo. Nullam posuere urna eu eros ultricies, vel aliquet massa viverra. Quisque a augue blandit purus ullamcorper ultrices. Donec molestie risus sit amet nisl vulputate, id consequat odio lobortis.</p>\r\n\r\n<ul>\r\n	<li>Sed nisi erat, luctus in malesuada sit amet, rhoncus non ligula.</li>\r\n	<li>Sed tortor nisi, gravida sit amet nisl in, ultrices sollicitudin tortor.</li>\r\n</ul>\r\n\r\n<p>Maecenas tempor justo at pellentesque euismod. Sed vel lacus sit amet sapien ultricies interdum et volutpat ex. Nunc faucibus lectus at justo pellentesque, at accumsan tellus ultricies.</p>\r\n\r\n<p>Sed vestibulum tempus pharetra. Duis at nibh at nisl laoreet imperdiet. Proin vitae mi vel enim dignissim ultrices et aliquet ex. Vestibulum sollicitudin tortor sagittis lacus commodo mattis. Aliquam augue diam, tempus non dolor tristique, varius commodo nibh.</p>\r\n','2016-11-18 18:33:47','2016-11-18 18:33:47'),(9,0,'Direito do Trabalho (Contencioso e Consultivo)','direito-do-trabalho-contencioso-e-consultivo','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt tortor sit amet est laoreet, a venenatis tellus suscipit. Etiam ut sapien sapien. Quisque vulputate, turpis vitae dictum facilisis, augue metus pulvinar elit, sagittis mollis orci sem ut ex. Morbi quis tristique sem.</p>\r\n\r\n<p><a href=\"#\">Exemplo de link</a></p>\r\n\r\n<p>Pellentesque ultricies sed nibh vel fringilla. Praesent et mauris congue, lobortis libero nec, dignissim leo. Nullam posuere urna eu eros ultricies, vel aliquet massa viverra. Quisque a augue blandit purus ullamcorper ultrices. Donec molestie risus sit amet nisl vulputate, id consequat odio lobortis.</p>\r\n\r\n<ul>\r\n	<li>Sed nisi erat, luctus in malesuada sit amet, rhoncus non ligula.</li>\r\n	<li>Sed tortor nisi, gravida sit amet nisl in, ultrices sollicitudin tortor.</li>\r\n</ul>\r\n\r\n<p>Maecenas tempor justo at pellentesque euismod. Sed vel lacus sit amet sapien ultricies interdum et volutpat ex. Nunc faucibus lectus at justo pellentesque, at accumsan tellus ultricies.</p>\r\n\r\n<p>Sed vestibulum tempus pharetra. Duis at nibh at nisl laoreet imperdiet. Proin vitae mi vel enim dignissim ultrices et aliquet ex. Vestibulum sollicitudin tortor sagittis lacus commodo mattis. Aliquam augue diam, tempus non dolor tristique, varius commodo nibh.</p>\r\n','2016-11-18 18:33:59','2016-11-18 18:33:59');
/*!40000 ALTER TABLE `areas_de_atuacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `artigos`
--

DROP TABLE IF EXISTS `artigos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artigos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artigos`
--

LOCK TABLES `artigos` WRITE;
/*!40000 ALTER TABLE `artigos` DISABLE KEYS */;
INSERT INTO `artigos` VALUES (1,1,'Título do Artigo','titulo-do-artigo','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt tortor sit amet est laoreet, a venenatis tellus suscipit. Etiam ut sapien sapien. Quisque vulputate, turpis vitae dictum facilisis, augue metus pulvinar elit, sagittis mollis orci sem ut ex. Morbi quis tristique sem. Pellentesque ultricies sed nibh vel fringilla. Praesent et mauris congue, lobortis libero nec, dignissim leo. Nullam posuere urna eu eros ultricies, vel aliquet massa viverra. Quisque a augue blandit purus ullamcorper ultrices. Donec molestie risus sit amet nisl vulputate, id consequat odio lobortis. Sed nisi erat, luctus in malesuada sit amet, rhoncus non ligula. Sed tortor nisi, gravida sit amet nisl in, ultrices sollicitudin tortor.</p>\r\n\r\n<ul>\r\n	<li>Maecenas tempor justo at pellentesque euismod.</li>\r\n	<li>Sed vel lacus sit amet sapien ultricies interdum et volutpat ex.</li>\r\n	<li>Nunc faucibus lectus at justo pellentesque, at accumsan tellus ultricies.</li>\r\n</ul>\r\n\r\n<p>Sed vestibulum tempus pharetra. Duis at nibh at nisl laoreet imperdiet. Proin vitae mi vel enim dignissim ultrices et aliquet ex. Vestibulum sollicitudin tortor sagittis lacus commodo mattis. Aliquam augue diam, tempus non dolor tristique, varius commodo nibh. Integer at sem semper, hendrerit velit vel, vehicula sapien. Duis ut elementum purus. Donec id tellus id magna ullamcorper imperdiet vitae in erat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum tincidunt lorem odio, sed congue tellus venenatis ut. Cras vehicula turpis quis velit cursus imperdiet. Donec placerat odio eu purus iaculis, at ultrices magna sagittis. Etiam sit amet convallis nibh.</p>\r\n','2016-11-18 18:34:25','2016-11-18 18:34:36'),(2,2,'Outro Exemplo com Título ocupando duas linhas no botão','outro-exemplo-com-titulo-ocupando-duas-linhas-no-botao','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt tortor sit amet est laoreet, a venenatis tellus suscipit. Etiam ut sapien sapien. Quisque vulputate, turpis vitae dictum facilisis, augue metus pulvinar elit, sagittis mollis orci sem ut ex. Morbi quis tristique sem. Pellentesque ultricies sed nibh vel fringilla. Praesent et mauris congue, lobortis libero nec, dignissim leo. Nullam posuere urna eu eros ultricies, vel aliquet massa viverra. Quisque a augue blandit purus ullamcorper ultrices. Donec molestie risus sit amet nisl vulputate, id consequat odio lobortis. Sed nisi erat, luctus in malesuada sit amet, rhoncus non ligula. Sed tortor nisi, gravida sit amet nisl in, ultrices sollicitudin tortor.</p>\r\n\r\n<ul>\r\n	<li>Maecenas tempor justo at pellentesque euismod.</li>\r\n	<li>Sed vel lacus sit amet sapien ultricies interdum et volutpat ex.</li>\r\n	<li>Nunc faucibus lectus at justo pellentesque, at accumsan tellus ultricies.</li>\r\n</ul>\r\n\r\n<p>Sed vestibulum tempus pharetra. Duis at nibh at nisl laoreet imperdiet. Proin vitae mi vel enim dignissim ultrices et aliquet ex. Vestibulum sollicitudin tortor sagittis lacus commodo mattis. Aliquam augue diam, tempus non dolor tristique, varius commodo nibh. Integer at sem semper, hendrerit velit vel, vehicula sapien. Duis ut elementum purus. Donec id tellus id magna ullamcorper imperdiet vitae in erat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum tincidunt lorem odio, sed congue tellus venenatis ut. Cras vehicula turpis quis velit cursus imperdiet. Donec placerat odio eu purus iaculis, at ultrices magna sagittis. Etiam sit amet convallis nibh.</p>\r\n','2016-11-18 18:34:30','2016-11-18 18:34:49');
/*!40000 ALTER TABLE `artigos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,1,'banner-home_20161118183646.png','Atuamos de forma consultiva e contenciosa, em todos os graus de jurisdição, administrativa ou judicial.','','2016-11-18 18:29:10','2016-11-18 18:36:47'),(2,3,'pexels-photo-24578_20161118182930.jpg','Exemplo com link','htttp://trupe.net','2016-11-18 18:29:30','2016-11-18 18:29:30'),(3,2,'pexels-photo-69040_20161118182938.png','','','2016-11-18 18:29:39','2016-11-18 18:29:39');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefones` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (1,'contato@trupe.net','11 2936 9144, 11 2936 9130','Rua Helena 309 cj. 81 - Vila Ol&iacute;mpia<br />\r\n04552-050 - S&atilde;o Paulo, SP','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.297865837111!2d-46.689231884515706!3d-23.593648084666803!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce574648355aab%3A0x1c7559524b969a7f!2sR.+Helena%2C+309+-+Vila+Olimpia%2C+S%C3%A3o+Paulo+-+SP%2C+04552-050!5e0!3m2!1spt-BR!2sbr!4v1479494194042\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>','#',NULL,'2016-11-18 18:36:38');
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
/*!40000 ALTER TABLE `contatos_recebidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contatos_recebidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `links`
--

DROP TABLE IF EXISTS `links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `links`
--

LOCK TABLES `links` WRITE;
/*!40000 ALTER TABLE `links` DISABLE KEYS */;
INSERT INTO `links` VALUES (1,0,'Trupe','http://trupe.net','2016-11-18 18:35:00','2016-11-18 18:35:39');
/*!40000 ALTER TABLE `links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2016_02_01_000000_create_users_table',1),('2016_03_01_000000_create_contato_table',1),('2016_03_01_000000_create_contatos_recebidos_table',1),('2016_11_18_020944_create_banners_table',1),('2016_11_18_021155_create_o_escritorio_table',1),('2016_11_18_021534_create_advogados_table',1),('2016_11_18_021754_create_links_table',1),('2016_11_18_022116_create_areas_de_atuacao_table',1),('2016_11_18_022148_create_artigos_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `o_escritorio`
--

DROP TABLE IF EXISTS `o_escritorio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `o_escritorio` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `chamada` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `o_escritorio`
--

LOCK TABLES `o_escritorio` WRITE;
/*!40000 ALTER TABLE `o_escritorio` DISABLE KEYS */;
INSERT INTO `o_escritorio` VALUES (1,'Escritório de advocacia especializado em direito empresarial, atuando de forma consultiva e contenciosa, em todos o graus de jurisdição, administrativa ou judicial.','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt tortor sit amet est laoreet, a venenatis tellus suscipit. Etiam ut sapien sapien. Quisque vulputate, turpis vitae dictum facilisis, augue metus pulvinar elit, sagittis mollis orci sem ut ex. Morbi quis tristique sem. Pellentesque ultricies sed nibh vel fringilla. Praesent et mauris congue, lobortis libero nec, dignissim leo. Nullam posuere urna eu eros ultricies, vel aliquet massa viverra. Quisque a augue blandit purus ullamcorper ultrices. Donec molestie risus sit amet nisl vulputate, id consequat odio lobortis. Sed nisi erat, luctus in malesuada sit amet, rhoncus non ligula. Sed tortor nisi, gravida sit amet nisl in, ultrices sollicitudin tortor. Maecenas tempor justo at pellentesque euismod. Sed vel lacus sit amet sapien ultricies interdum et volutpat ex. Nunc faucibus lectus at justo pellentesque, at accumsan tellus ultricies.</p>\r\n\r\n<p>Sed vestibulum tempus pharetra. Duis at nibh at nisl laoreet imperdiet. Proin vitae mi vel enim dignissim ultrices et aliquet ex. Vestibulum sollicitudin tortor sagittis lacus commodo mattis. Aliquam augue diam, tempus non dolor tristique, varius commodo nibh. Integer at sem semper, hendrerit velit vel, vehicula sapien. Duis ut elementum purus. Donec id tellus id magna ullamcorper imperdiet vitae in erat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum tincidunt lorem odio, sed congue tellus venenatis ut. Cras vehicula turpis quis velit cursus imperdiet. Donec placerat odio eu purus iaculis, at ultrices magna sagittis. Etiam sit amet convallis nibh.</p>\r\n\r\n<p>Aliquam condimentum ut ante et ultricies. Fusce et dolor magna. Nullam consectetur consequat mollis. Nulla ac dui ante. Phasellus et odio at justo consectetur tempor ac nec purus. Nam a ultrices sem. Mauris viverra, nisi vitae interdum mollis, turpis urna finibus sem, nec mollis ipsum velit vitae libero. Duis sed lacus mauris. Nunc metus odio, sodales in pellentesque sed, faucibus a lectus. Cras consectetur efficitur est. Morbi purus tortor, convallis et diam ut, vehicula efficitur velit. Nulla urna sapien, congue consequat aliquet ut, tempor ut diam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.</p>\r\n','img-quemsomos_20161118183105.png',NULL,'2016-11-18 18:31:05');
/*!40000 ALTER TABLE `o_escritorio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net','$2y$10$V1B3CdDbsdA81b4vduPt..pHTEKxiL8raUr8W9YLG/A7FetgBpR8y','DYAaRS5hWLya9nD3rNktGCioXjeCmq3yeEWCORW7KvQ44zrgoUcdlAtBsFNV',NULL,'2016-11-18 18:28:29');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-18 18:37:25
