<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\OEscritorioRequest;
use App\Http\Controllers\Controller;

use App\Models\OEscritorio;

class OEscritorioController extends Controller
{
    public function index()
    {
        $registro = OEscritorio::first();

        return view('painel.o-escritorio.edit', compact('registro'));
    }

    public function update(OEscritorioRequest $request, OEscritorio $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = OEscritorio::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.o-escritorio.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
