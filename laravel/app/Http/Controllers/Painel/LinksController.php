<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\LinksRequest;
use App\Http\Controllers\Controller;

use App\Models\Link;

class LinksController extends Controller
{
    public function index()
    {
        $registros = Link::ordenados()->get();

        return view('painel.links.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.links.create');
    }

    public function store(LinksRequest $request)
    {
        try {

            $input = $request->all();


            Link::create($input);
            return redirect()->route('painel.links.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Link $registro)
    {
        return view('painel.links.edit', compact('registro'));
    }

    public function update(LinksRequest $request, Link $registro)
    {
        try {

            $input = $request->all();


            $registro->update($input);
            return redirect()->route('painel.links.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Link $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.links.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
