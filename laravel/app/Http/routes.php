<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('o-escritorio', 'OEscritorioController@index')->name('o-escritorio');
    Route::get('advogados', 'AdvogadosController@index')->name('advogados');
    Route::get('areas-de-atuacao/{atuacao_slug?}', 'AreasDeAtuacaoController@index')->name('areas-de-atuacao');
    Route::get('artigos/{artigo_slug?}', 'ArtigosController@index')->name('artigos');
    Route::get('links', 'LinksController@index')->name('links');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');


    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('artigos', 'ArtigosController');
		Route::resource('areas-de-atuacao', 'AreasDeAtuacaoController');
		Route::resource('links', 'LinksController');
		Route::resource('advogados', 'AdvogadosController');
		Route::resource('o-escritorio', 'OEscritorioController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
