@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="banners">
            @foreach($banners as $banner)
                @if($banner->link)
                <a href="{{ $banner->link }}" class="slide" style="background-image:url('{{ asset('assets/img/banners/'.$banner->imagem) }}')">
                    @if($banner->frase)
                    <p>{{ $banner->frase }}</p>
                    @endif
                </a>
                @else
                <div class="slide" style="background-image:url('{{ asset('assets/img/banners/'.$banner->imagem) }}')">
                    @if($banner->frase)
                    <p>{{ $banner->frase }}</p>
                    @endif
                </div>
                @endif
            @endforeach
            <div class="cycle-pager"></div>
        </div>

        <div class="center">
            <div class="atuacao">
                <h2>ÁREAS DE ATUAÇÃO</h2>

                <div>
                    @foreach($areasDeAtuacao as $titulo => $slug)
                    <a href="{{ route('areas-de-atuacao', $slug) }}">
                        <div class="wrapper">
                            <span>{{ $titulo }}</span>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
