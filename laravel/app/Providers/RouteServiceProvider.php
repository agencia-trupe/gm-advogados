<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('artigos', 'App\Models\Artigo');
		$router->model('areas-de-atuacao', 'App\Models\AreaDeAtuacao');
		$router->model('links', 'App\Models\Link');
		$router->model('advogados', 'App\Models\Advogado');
		$router->model('o-escritorio', 'App\Models\OEscritorio');
		$router->model('banners', 'App\Models\Banner');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('atuacao_slug', function($value) {
            return \App\Models\AreaDeAtuacao::whereSlug($value)->first() ?: abort('404');
        });

        $router->bind('artigo_slug', function($value) {
            return \App\Models\Artigo::whereSlug($value)->first() ?: abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
