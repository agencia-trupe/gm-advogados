@extends('frontend.common.template')

@section('content')

    <div class="main links">
        <div class="center">
            <h2>LINKS</h2>

            <img src="{{ asset('assets/img/layout/img-links.png') }}" alt="">

            <div class="lista">
                @foreach($links as $link)
                <a href="{{ $link->link }}" class="link-aside">{{ $link->titulo }}</a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
