<?php

return [

    'name'        => 'G&M Advogados',
    'title'       => 'G&M &middot; Galrão e Machtans Advogados',
    'description' => 'Escritório de advocacia especializado em direito empresarial, atuando de forma consultiva e contenciosa, em todos o graus de jurisdição, administrativa ou judicial.',
    'keywords'    => 'advocacia, direito do trabalho, direito civil, direito contratual, direito societário, escritório de advocacia, treinamento em recursos humanos, recuperação de crédito, advogado, direito de familia, direito securitário',
    'share_image' => '',
    'analytics'   => null

];
