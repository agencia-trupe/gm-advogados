@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Advogados /</small> Editar Advogado</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.advogados.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.advogados.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
