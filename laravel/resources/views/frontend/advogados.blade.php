@extends('frontend.common.template')

@section('content')

    <div class="main advogados">
        <div class="center">
            <h2>ADVOGADOS</h2>

            @foreach($advogados as $advogado)
            <div class="advogado">
                <div class="info">
                    <h3>{{ $advogado->nome }}</h3>
                    <p>
                        E-mail:
                        <a href="mailto:{{ $advogado->e_mail }}">{{ $advogado->e_mail }}</a>
                    </p>
                    @if($advogado->telefone)
                    <p>Tel: {{ $advogado->telefone }}</p>
                    @endif
                    @if($advogado->celular)
                    <p>Cel: {{ $advogado->celular }}</p>
                    @endif
                </div>

                <div class="texto">
                    {!! $advogado->texto !!}
                </div>
            </div>
            @endforeach
        </div>
    </div>

@endsection
