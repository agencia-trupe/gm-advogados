<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\AreaDeAtuacao;

class AreasDeAtuacaoController extends Controller
{
    public function index(AreaDeAtuacao $area)
    {
        $selecao = $area->exists ? $area : AreaDeAtuacao::ordenados()->first();
        if (!$selecao) abort('404');

        return view('frontend.areas-de-atuacao')->with([
            'areasDeAtuacao' => AreaDeAtuacao::ordenados()->lists('slug', 'titulo'),
            'selecao'        => $selecao
        ]);
    }
}
