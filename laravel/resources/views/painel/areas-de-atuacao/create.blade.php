@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Áreas de Atuação /</small> Adicionar Área de Atuação</h2>
    </legend>

    {!! Form::open(['route' => 'painel.areas-de-atuacao.store', 'files' => true]) !!}

        @include('painel.areas-de-atuacao.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
