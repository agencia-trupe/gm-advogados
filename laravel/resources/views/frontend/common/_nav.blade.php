<a href="{{ route('o-escritorio') }}" @if(Route::currentRouteName() === 'o-escritorio') class="active" @endif>O Escritório</a>
<a href="{{ route('advogados') }}" @if(Route::currentRouteName() === 'advogados') class="active" @endif>Advogados</a>
<a href="{{ route('areas-de-atuacao') }}" @if(Route::currentRouteName() === 'areas-de-atuacao') class="active" @endif>Áreas de Atuação</a>
<a href="{{ route('artigos') }}" @if(Route::currentRouteName() === 'artigos') class="active" @endif>Artigos</a>
<a href="{{ route('links') }}" @if(Route::currentRouteName() === 'links') class="active" @endif>Links</a>
<a href="{{ route('contato') }}" @if(Route::currentRouteName() === 'contato') class="active" @endif>Contato</a>
