    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ config('site.name') }}</a>
        </div>
        <nav>
            <div class="center">
                @include('frontend.common._nav')
            </div>
        </nav>
    </header>
