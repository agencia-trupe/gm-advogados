<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdvogadosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'e_mail' => 'required|email',
            'telefone' => '',
            'celular' => '',
            'texto' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
