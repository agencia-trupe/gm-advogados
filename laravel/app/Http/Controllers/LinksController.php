<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Link;

class LinksController extends Controller
{
    public function index()
    {
        $links = Link::ordenados()->get();

        return view('frontend.links', compact('links'));
    }
}
