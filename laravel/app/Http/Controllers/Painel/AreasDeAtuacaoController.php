<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AreasDeAtuacaoRequest;
use App\Http\Controllers\Controller;

use App\Models\AreaDeAtuacao;

class AreasDeAtuacaoController extends Controller
{
    public function index()
    {
        $registros = AreaDeAtuacao::ordenados()->get();

        return view('painel.areas-de-atuacao.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.areas-de-atuacao.create');
    }

    public function store(AreasDeAtuacaoRequest $request)
    {
        try {

            $input = $request->all();


            AreaDeAtuacao::create($input);
            return redirect()->route('painel.areas-de-atuacao.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(AreaDeAtuacao $registro)
    {
        return view('painel.areas-de-atuacao.edit', compact('registro'));
    }

    public function update(AreasDeAtuacaoRequest $request, AreaDeAtuacao $registro)
    {
        try {

            $input = $request->all();


            $registro->update($input);
            return redirect()->route('painel.areas-de-atuacao.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(AreaDeAtuacao $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.areas-de-atuacao.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
