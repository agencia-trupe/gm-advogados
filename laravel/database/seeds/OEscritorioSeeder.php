<?php

use Illuminate\Database\Seeder;

class OEscritorioSeeder extends Seeder
{
    public function run()
    {
        DB::table('o_escritorio')->insert([
            'chamada' => '',
            'texto' => '',
            'imagem' => '',
        ]);
    }
}
