<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Models\AreaDeAtuacao;

class HomeController extends Controller
{
    public function index()
    {
        $banners = Banner::ordenados()->get();
        $areasDeAtuacao = AreaDeAtuacao::ordenados()->lists('slug', 'titulo');

        return view('frontend.home', compact('banners', 'areasDeAtuacao'));
    }
}
