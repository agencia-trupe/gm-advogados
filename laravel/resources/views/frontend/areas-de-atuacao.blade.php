@extends('frontend.common.template')

@section('content')

    <div class="main atuacao-artigos">
        <div class="center">
            <h2>ÁREAS DE ATUAÇÃO</h2>

            <div class="lista">
                @foreach($areasDeAtuacao as $titulo => $slug)
                <a href="{{ route('areas-de-atuacao', $slug) }}" class="link-aside @if($selecao-> slug === $slug) active @endif">
                    {{ $titulo }}
                </a>
                @endforeach
            </div>

            <div class="texto">
                {!! $selecao->texto !!}
            </div>
        </div>
    </div>

@endsection
