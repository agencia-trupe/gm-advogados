<ul class="nav navbar-nav">
	<li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.banners.index') }}">Banners</a>
	</li>
    <li @if(str_is('painel.o-escritorio*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.o-escritorio.index') }}">O Escritório</a>
    </li>
    <li @if(str_is('painel.advogados*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.advogados.index') }}">Advogados</a>
    </li>
    <li @if(str_is('painel.areas-de-atuacao*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.areas-de-atuacao.index') }}">Áreas de Atuação</a>
    </li>
    <li @if(str_is('painel.artigos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.artigos.index') }}">Artigos</a>
    </li>
    <li @if(str_is('painel.links*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.links.index') }}">Links</a>
    </li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
</ul>
