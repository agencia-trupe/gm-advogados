@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Advogados /</small> Adicionar Advogado</h2>
    </legend>

    {!! Form::open(['route' => 'painel.advogados.store', 'files' => true]) !!}

        @include('painel.advogados.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
