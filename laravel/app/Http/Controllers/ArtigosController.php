<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Artigo;

class ArtigosController extends Controller
{
    public function index(Artigo $artigo)
    {
        $selecao = $artigo->exists ? $artigo : Artigo::ordenados()->first();
        if (!$selecao) abort('404');

        return view('frontend.artigos')->with([
            'artigos' => Artigo::ordenados()->lists('slug', 'titulo'),
            'selecao' => $selecao
        ]);
    }
}
