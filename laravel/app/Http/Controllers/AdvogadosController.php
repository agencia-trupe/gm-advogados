<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Advogado;

class AdvogadosController extends Controller
{
    public function index()
    {
        $advogados = Advogado::ordenados()->get();

        return view('frontend.advogados', compact('advogados'));
    }
}
