@extends('painel.common.template')

@section('content')

    <legend>
        <h2>O Escritório</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.o-escritorio.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.o-escritorio.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
