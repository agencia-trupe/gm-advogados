<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AdvogadosRequest;
use App\Http\Controllers\Controller;

use App\Models\Advogado;

class AdvogadosController extends Controller
{
    public function index()
    {
        $registros = Advogado::ordenados()->get();

        return view('painel.advogados.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.advogados.create');
    }

    public function store(AdvogadosRequest $request)
    {
        try {

            $input = $request->all();


            Advogado::create($input);
            return redirect()->route('painel.advogados.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Advogado $registro)
    {
        return view('painel.advogados.edit', compact('registro'));
    }

    public function update(AdvogadosRequest $request, Advogado $registro)
    {
        try {

            $input = $request->all();


            $registro->update($input);
            return redirect()->route('painel.advogados.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Advogado $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.advogados.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
