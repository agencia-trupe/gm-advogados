<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\OEscritorio;

class OEscritorioController extends Controller
{
    public function index()
    {
        $escritorio = OEscritorio::first();

        return view('frontend.o-escritorio', compact('escritorio'));
    }
}
