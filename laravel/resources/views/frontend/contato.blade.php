@extends('frontend.common.template')

@section('content')

    <div class="main contato">
        <div class="center">
            <h2>CONTATO</h2>

            <div class="info">
                <div class="box">
                    <p class="telefones">
                        @foreach(Tools::telefones($contato->telefones) as $telefone)
                        <span>{{ $telefone }}</span>
                        @endforeach
                    </p>

                    <p class="endereco">{!! $contato->endereco !!}</p>
                </div>

                <div class="mapa">
                    {!! $contato->google_maps !!}
                </div>
            </div>

            <div class="formulario-contato">
                <h4>FALE CONOSCO</h4>
                <form action="" id="form-contato" method="POST">
                    <input type="text" name="nome" id="nome" placeholder="nome" required>
                    <input type="email" name="email" id="email" placeholder="e-mail" required>
                    <input type="text" name="telefone" id="telefone" placeholder="telefone">
                    <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                    <input type="submit" value="ENVIAR">
                    <div id="form-contato-response"></div>
                </form>
            </div>
        </div>
    </div>

@endsection
